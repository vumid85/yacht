<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Administration pages translate
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'site_title' => '요트 예약',
    'logout' => '로그 아웃',
    //sidebar
    'owner_management' => '소유자 관리',
    'product_management' => '제품 관리',
    'yacht_management' => '요트 관리',
    'membership_info' => '회원 정보',
    'member_list' => '회원 목록',
    'reservations' => '예약',
    'reservation_list' => '예약 목록',



    //dashboard
    'dashboard'=>'대시 보드',
    'share' => '공유',
    'export' => '수출',
    'this_week' => '이번 주',

    //vendors.index
    'pindex_vendor_list_title' => '요트 소유자',
    'pindex_name'=>'이름',
    'pindex_representative' => '대표자 명',
    'pindex_contact' => '연락처',
    'pindex_email' => '이메일',
    'pindex_activate' => '활성',
    'pindex_active' => '활성',
    'pindex_view_yacht' => '요트보기',
    'pindex_agree_activate' => '소유자를 활성 하시겠습니까?',
	
	//yacht.index,add
    'yacht_list' =>'요트 목록',
    'yacht_name' => '명',
    'yacht_company' => '회사 명',
    'yacht_address' => '주소',
    'yacht_capacity' => '수용력',
    'yacht_add' => '추가',
    'yacht_info' => '요트 정보 변경',
    'yacht_select' => '선택 해주세요...',
    'yacht_load_images' => '이미지로드...',
    'yacht_length' => '길이',
    'yacht_activity' => '활동',
    'yacht_start_activity' => '활동 시작',
    'yacht_end_activity' => '활동 종료',
    'yacht_allday' => '종일',

    'yacht_regularity' => '정격',
    'yacht_regular' => '정규병',
    'yacht_weekdays' => '평일',
    'yacht_weekends' => '주말',
    'yacht_bydays' => '일별',

    //product.index, add
    'product_name' => '명',
    'product_division' => '분할',
    'product_displayed' => '표시',
    'product_capacity' => '용량 성인 / 어린이',
    'product_description' => '설명',
    'product_adult_price' => '성인 가격',
    'product_child_price' => '어린이 가격',

    'product_info' => '상품 정보',
    'product_tour' => '여행',
    'product_lodgement' => '거점',
    'product_theme' => '테마',
    'product_adults' => '성인',
    'product_children' => '어린이',
    'product_price' => '가격',
    'product_brief' => '간단한 소개',
    'product_area' => '지역',
    'product_location' => '위치',
    'product_reservation_time' => '예약 시간',
    'product_reservation_add' => '추가',
    'product_reservation_remove' => '제거',
    'product_save' => '저장',


];