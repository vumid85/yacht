<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '이 자격 증명은 당사 기록과 일치하지 않습니다.',
    'throttle' => '로그인 시도가 너무 많습니다. 다시 시도하십시오..',
    'login_title' => '로그인',
    'email' => '이메일 주소',
    'password' => '암호',
    'remember' => '자동로그인',
    'forgot_password' => '비밀번호 찾기'


];
